package com.mbjproject.web.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mbjproject.web.dao.ContactUsDao;
import com.mbjproject.web.model.ContactUs;

public class ContactUsController extends HttpServlet {	
	private String REQUEST_STATUS = "requestStatus";
	private String RESPONSE_SUCCESS_1 = "Thanks for contacting us, ";
	private String RESPONSE_SUCCESS_2 = "! We will revert back to you shortly.";
	private String RESPONSE_FAILURE = "Unable to record your response. Please try again later!";

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("requestType").equals("store")) {
			String requestStatus = null;
			
			ContactUs contactRequest = new ContactUs();
			contactRequest.setName(request.getParameter("name"));
			contactRequest.setEmail(request.getParameter("email"));
			contactRequest.setMessage(request.getParameter("message"));
			contactRequest.setActive(true);

			ContactUsDao requestDao = new ContactUsDao();
			
			if(requestDao.storeRequest(contactRequest) == 1)
				requestStatus = RESPONSE_SUCCESS_1 + request.getParameter("name") + RESPONSE_SUCCESS_2;
				
			else
				requestStatus = RESPONSE_FAILURE;
			
			request.setAttribute(REQUEST_STATUS, requestStatus);
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("contactus");
			requestDispatcher.forward(request, response);
		}
		
		if(request.getParameter("requestType").equals("archive")) {
			int id = Integer.parseInt(request.getParameter("requestId"));
			
			ContactUsDao requestDao = new ContactUsDao();
			requestDao.archiveRequest(id);
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("contactus/requests");
			requestDispatcher.forward(request, response);
		}
	}
}
