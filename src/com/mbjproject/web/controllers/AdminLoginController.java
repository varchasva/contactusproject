package com.mbjproject.web.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mbjproject.web.dao.AdminDao;
import com.mbjproject.web.model.Admin;

public class AdminLoginController extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		Admin admin = new Admin();
		admin.setUsername(username);
		admin.setPassword(password);
		
		AdminDao adminDao = new AdminDao();
		
		try {
			if(adminDao.validateAdmin(admin)) {
				HttpSession session = request.getSession();
				session.setAttribute("username", username);
				session.setAttribute("password", password);
				
				RequestDispatcher rd = request.getRequestDispatcher("/admin/contactus/requests");
				rd.forward(request, response);
			}
			
			else {
				response.sendRedirect("login");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
