package com.mbjproject.web.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ContactUsConnection {
	public static Connection getConnection() {
		Connection conn = null;
		
		String url = "jdbc:postgresql://localhost:5432/contactDB";
		String username = "postgres";
		String password = "root";
		
		try {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(url, username, password);
		} catch(SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return conn;
	}
}
