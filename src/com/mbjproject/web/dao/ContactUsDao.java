package com.mbjproject.web.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.mbjproject.web.connections.ContactUsConnection;
import com.mbjproject.web.model.ContactUs;

public class ContactUsDao {
	public int storeRequest(ContactUs contactRequest) {
		int rowsAffected = 0;
		
		String query = "insert into contacts (contactName, contactEmail, contactMessage, isActive) values(?, ?, ?, ?)";
		
		Connection conn = ContactUsConnection.getConnection();
		
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, contactRequest.getName());
			ps.setString(2,contactRequest.getEmail());
			ps.setString(3, contactRequest.getMessage());
			ps.setBoolean(4, contactRequest.isActive());
				
			rowsAffected = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rowsAffected;
	}
	
	public void archiveRequest(int id) {
		String query = "update contacts set isActive=false where contactId=?";
		
		Connection conn = ContactUsConnection.getConnection();
			
		try {
			PreparedStatement ps = conn.prepareStatement(query);	
			ps.setInt(1, id);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<ContactUs> getRequests(boolean isActiveRequest) {
		List<ContactUs> activeRequests = null;
		List<ContactUs>  archivedRequests = null;
		
		if(isActiveRequest) {
			String query = "select *from contacts where isActive=true";
			
			activeRequests = new ArrayList<ContactUs>();
			
			ResultSet rs = null;
			
			Connection conn = ContactUsConnection.getConnection();
				
			try {
				PreparedStatement ps = conn.prepareStatement(query);		

				rs = ps.executeQuery();
					
				while(rs.next()) {
					ContactUs contactRequest = new ContactUs();
					contactRequest.setId(rs.getInt(1));
					contactRequest.setName(rs.getString(2));
					contactRequest.setEmail(rs.getString(3));
					contactRequest.setMessage(rs.getString(4));
					contactRequest.setActive(rs.getBoolean(5));
						
					activeRequests.add(contactRequest);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return  activeRequests;
		}
		
		else {
			String query = "select *from contacts where isActive=false";
			
			archivedRequests = new ArrayList<ContactUs>();
			
			ResultSet rs = null;
			
			Connection conn = ContactUsConnection.getConnection();
				
			try {
				Statement st = conn.createStatement();		

				rs = st.executeQuery(query);
					
				while(rs.next()) {				
					ContactUs contactRequest = new ContactUs();
					contactRequest.setId(rs.getInt(1));
					contactRequest.setName(rs.getString(2));
					contactRequest.setEmail(rs.getString(3));
					contactRequest.setMessage(rs.getString(4));
					contactRequest.setActive(rs.getBoolean(5));

					archivedRequests.add(contactRequest);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		return  archivedRequests;
		}
	}
}
