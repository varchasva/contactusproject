package com.mbjproject.web.dao;

import java.sql.*;

import com.mbjproject.web.connections.AdminConnection;
import com.mbjproject.web.model.Admin;


public class AdminDao {
	public boolean validateAdmin(Admin admin) throws SQLException {
		String username = admin.getUsername();
		String password = admin.getPassword();	
		String query = "select * from admins where adminUsername=? and adminPassword=?";
		
		Connection conn = AdminConnection.getConnection();
		
		PreparedStatement ps = conn.prepareStatement(query);
		
		ps.setString(1, username);
		ps.setString(2, password);
			
		ResultSet rs = ps.executeQuery();
			
		return rs.next();
	}
}
