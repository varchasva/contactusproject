<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<style>
			#btn {
				border: solid white 1px;
				border-radius: 4px;
				background-color: #e7e7e7; color: black;
				height: 30px;
			}
			#btn:hover {
				cursor: pointer;
				color: #4290f5;
			}
			fieldset {
				height: 155px;
			}
			form {
				display: flex;
				flex-direction: column;
				width: 20%;
			}
			.labels {
				display: flex;
				flex-direction: column;
				margin: 5px 0px 0px 0px;
			}
			.loginDiv {
				display: flex;
				flex-direction: row;
				margin: 20px 0px 0px 0px;
			}
			.textboxes {
				display: flex;
				flex-direction: column;
				margin: 10px 0px 0px 0px;
			}
		</style>
	</head>
	<body>
		<%
			response.setHeader("Pragma","no-cache");
			response.setHeader("Cache-Control","no-store");
			response.setHeader("Expires","0");
			response.setDateHeader("Expires",-1);
		%>
		
		<form action="loginController" method="post">
			<fieldset>
				<div class="nameDiv">
					<label for="username" class='labels'>Admin username</label>
					<input type="text" name="username" id="username" class='textboxes' required>
				</div>
				<div class="passDiv">
					<label for="password" class='labels'>Admin password</label>
					<input type="password" name="password" id="password" class='textboxes' required>
				</div>
				<div class="loginDiv">
					<input type="submit" id='btn' value="LOGIN">
				</div>
			</fieldset>
		</form>
	</body>
</html>