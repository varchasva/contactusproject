<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.mbjproject.web.model.ContactUs,com.mbjproject.web.dao.ContactUsDao,
    java.util.List,java.util.ArrayList,javax.servlet.RequestDispatcher"%>

<!DOCTYPE html>
	<html>
		<head>
			<style>
				#heading1 {
					color: #72ed4c;
				}
				#heading2 {
					color: #e0251b;
				}
				a:hover {
					color: #f59942;
				}
				table, th, td {
		  			border: 1px solid goldenrod;
		  			border-collapse: collapse;
		  			background-color: lemonchiffon;
		  			text-align: left;
				}
				th, td {
					padding: 8px;
				}
				th {
		  			text-align: left;
		  			background-color: goldenrod;
				}
		
		</style>
	</head>
	<body>
		<%
			if(session.getAttribute("username") == null)
				response.sendRedirect("/MountBlueJSP/admin/login");
		%>
		
		<%
			response.setHeader("Cache-Control","no-cache"); 
			response.setHeader("Cache-Control","no-store"); 
			response.setDateHeader("Expires", -1); 
			response.setHeader("Pragma","no-cache");
		%>
		
		<%!
			private boolean IS_ACTIVE_REQUEST = true;
			private boolean IS_ARCHIVED_REQUEST = false;
			
			private String REQUEST_ID  = "RequestID";
			private String REQUEST_NAME = "RequestName";
			private String REQUEST_EMAIL = "RequestEmail";
			private String REQUEST_MESSAGE = "RequestMessage";
		%>
		
		<h1 id='heading1'>Active Requests: </h1>
		<%
			ContactUsDao requestDao = new ContactUsDao();
			
			List<ContactUs> activeRequests = requestDao.getRequests(IS_ACTIVE_REQUEST);
			
			int id = 0;
			
			for(ContactUs contactRequest: activeRequests) {
				id = contactRequest.getId();
				String name = contactRequest.getName();
				String email = contactRequest.getEmail();
				String message = contactRequest.getMessage();
				
				boolean isActive = contactRequest.isActive();
				
				out.println("<table>");
				out.println("<tr>");
				out.println("<th>"+REQUEST_ID+"</th>");
				out.println("<th>"+REQUEST_NAME+"</th>");
				out.println("<th>"+REQUEST_EMAIL+"</th>");
				out.println("<th>"+REQUEST_MESSAGE+"</th>");
				out.println("</tr>");
				
				out.println("<tr id=\"activeRequest"+id+"\">");
				out.println("<td>"+id+"</td>");
				out.println("<td>"+name+"</td>");
				out.println("<td>"+email+"</td>");
				out.println("<td>"+message+"</td>");
				out.println("<td><button id='"+id+"' onClick='archiveRecord("+id+")'>Archive this!</button></td>");
				out.println("</tr>");
				out.println("</table>");
				out.println("<br><br>");
			}
		%>
		
		<h1 id='heading2'>Archived Requests: </h1>
		<%
			List<ContactUs> archivedRequests = requestDao.getRequests(IS_ARCHIVED_REQUEST);
			
			for(ContactUs contactRequest: archivedRequests) {
				id = contactRequest.getId();
				String name = contactRequest.getName();
				String email = contactRequest.getEmail();
				String message = contactRequest.getMessage();
				
				out.println("<table>");
				out.println("<tr>");
				out.println("<th>"+REQUEST_ID+"</th>");
				out.println("<th>"+REQUEST_NAME+"</th>");
				out.println("<th>"+REQUEST_EMAIL+"</th>");
				out.println("<th>"+REQUEST_MESSAGE+"</th>");
				out.println("</tr>");
				
				out.println("<tr>");
				out.println("<td>"+id+"</td>");
				out.println("<td>"+name+"</td>");
				out.println("<td>"+email+"</td>");
				out.println("<td>"+message+"</td>");
				out.println("</tr>");
				out.println("</table>");
				out.println("<br><br>");
			}
		%>
		
		<form action="archiveController" method="post" id="archiveForm">
			<input type="hidden" name="requestType" value="archive">
			<input type="hidden" name="requestId" id="requestId">
		</form>
		
		<script>
			function archiveRecord(id) {
				var archiveForm = document.getElementById("archiveForm")
				archiveForm.elements["requestId"].value = id;
				archiveForm.submit();
			}
		</script>
		
		<a href="/MountBlueJSP/index.jsp">Home</a>
	</body>
</html>