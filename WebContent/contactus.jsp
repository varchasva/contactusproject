<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<style>
		a:hover {
			color: #f59942;
		}
		body {
			display: flex;
			flex-direction: column;
			align-items: center;
			background-color: #f5f5f5;
		}
		.box {
			display: flex;
			flex-direction: column;
			align-items: flex-start;
			width: 400px;
			height: 500px;
			background-color: #ffffff;
		}
		#btn {
			border: solid white 1px;
			border-radius: 4px;
			background-color: #e7e7e7; color: black;
			height: 40px;
			width: 80px;
		}
		#btn:hover {
			cursor: pointer;
			color: #4290f5;
		}
		.btnDiv {
			margin:auto;
			padding-top: 30px;
		}
		#email {
			height: 40%;
		}
		.emailDiv{
			width: 90%;
			height: 100%;
		}
		form {
			display: flex;
			flex-direction: column;
			margin: auto;
			background-color: #FFFFFF;
			width: 100%;
			align-items: center;
			height: 60%;
		}
		.heading {
			display: flex;
			flex-direction: column;
			background-color: #dbdbdb;
			width: 395px;
			margin: 3px;
		}
		.labels {
			display: flex;
			flex-direction: column;
		}
		.linkDiv {
			margin: auto;
			padding-top: 30px;
		}
		#message {
			height: 60%;
		}
		.messageDiv {
			width: 90%;
			height: 100%;
		}
		#name {
			height: 40%;
		}
		.nameDiv{
			width: 90%;
			height: 100%;
		}
		.textboxes {
			display: flex;
			flex-direction: column;
			width: 100%;
		}
	</style>
</head>
<body>
	<%
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",-1);
	%>
	
	<%!
		private String RESPONSE_FAILURE = "Unable to record your response. Please try again later!";
	%>
	
	<div class='box'>
		<div class='heading'>
			<h1>Contact Us</h1>
			<p>Please fill this form in a decent manner</p>
		</div>
	<form action="contactUsController" method="post">
		<div class='nameDiv'>
			<label for="name" class="labels">Full Name</label>
			<input type="text" name="name" id="name" class="textboxes" required>
		</div>
		<div class='emailDiv' class="labels">
			<label for="email">E-mail</label>
			<input type="email" name="email" id="email" class="textboxes" required>
		</div>
		<div class='messageDiv' class="labels">
			<label for="message">Message</label>
			<textarea name="message" id="message" class="textboxes" required></textarea><br>
		</div>
		<div class='btnDiv'>
			<input type="submit" id="btn" value="SUBMIT">
		</div>
		<input type="hidden" name="requestType" value="store">
	</form>
	</div>
		<div class='linkDiv'>
		<a href="index.jsp">Home</a>
	</div>
	
	<%
		String requestStatus = (String)request.getAttribute("requestStatus");
					
		if(requestStatus != null) {
			if(!requestStatus.equals(RESPONSE_FAILURE)) {
				out.println("<p style='color: #317d45;'>"+requestStatus+"</p>");
			}
			else {
				out.println("<p style='color: #b81111;'>"+requestStatus+"</p>");
			}
		}
		
		session.removeAttribute("requestStatus");
		session.invalidate();
	%>
</body>
</html>