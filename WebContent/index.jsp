<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<style>
			a:hover{
				color: #f59942;
			}
		</style>
	</head>
	<body>
		<%
			response.setHeader("Pragma","no-cache");
			response.setHeader("Cache-Control","no-store");
			response.setHeader("Expires","0");
			response.setDateHeader("Expires",-1);
		%>
		
		<a href="admin/login" class="login">Admin Login</a><br><br>
		<a href="contactus" class="contact">Contact Us</a>
	</body>
</html>